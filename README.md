# PhoneGap and Test Runner:
The code produced by student project from NITH 2012.
TestRunner is a framework for doing Selenium-style functional (UI) tests on PhoneGap-apps.
TaptOgFunnet is a mobile app for Android and iOS developed with PhoneGap and TestRunner, it was not completed.
The code comes from 6 projects, so I did not clone but made a full structure:
 * https://github.com/rhelgeby/TestRunner
 * https://github.com/rhelgeby/TestRunner-Core
 * https://github.com/rhelgeby/TestRunner-Demo
 * https://github.com/rhelgeby/TaptOgFunnetApp-Core
 * https://github.com/rhelgeby/TaptOgFunnet-Android
 * https://github.com/rhelgeby/TaptOgFunnet-iOS

The hands-on session was performed according to script in https://docs.google.com/a/nith.no/document/d/1Tx0PKKxvIF92aZqjRsyMmkNUusteOYJrI4PiO_7aPLw/edit
I downloaded the script as a PDF, along with the zip file referred to in script.

